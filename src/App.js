import React from "react";
import ControlPanel from "./View/components/ControlPanel/ControlPanel";

function App() {
  return <ControlPanel />;
}

export default App;
