import React from "react";
import { MuiThemeProvider } from "@material-ui/core/styles";
import { ThemeProvider } from "styled-components";
import CssBaseline from "@material-ui/core/CssBaseline";
import App from "./App";
import theme from "./View/style/theme";

class Launcher extends React.PureComponent {
  render() {
    return (
      <>
        <MuiThemeProvider theme={theme}>
          <ThemeProvider theme={theme}>
            <CssBaseline />
            <App />
          </ThemeProvider>
        </MuiThemeProvider>
      </>
    );
  }
}

export default Launcher;
