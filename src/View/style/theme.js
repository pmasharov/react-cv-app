import { createMuiTheme } from "@material-ui/core/styles";
import purple from "@material-ui/core/colors/purple";
import green from "@material-ui/core/colors/green";
import "./fonts/fonts.css";

const defaultFontFamily = ["Arial", "VictorMono", "Monoid", "Mono"].join(",");

const primaryColor = purple[300];
const secondaryColor = green[300];

const theme = createMuiTheme({
  palette: {
    primary: {
      main: primaryColor
    },
    secondary: {
      main: secondaryColor
    }
  },
  typography: {
    fontFamily: defaultFontFamily
  },
  overrides: {
    MuiCssBaseline: {
      "@global": {
        body: {
          fontFamily: defaultFontFamily
        }
      }
    },
    MuiButton: {
      // text: {
      //   fontFamily: "inherit",
      //   padding: ".25rem .5rem"
      // },
      root: {
        // fontSize: ".75rem"
        // "&:hover": {
        backgroundColor: primaryColor
        // }
      }
      // label: {
      //   border: "solid 1px black"
      // }
    },
    MuiButtonBase: {
      root: {
        backgroundColor: secondaryColor
      }
    }
  }
});

export default theme;
