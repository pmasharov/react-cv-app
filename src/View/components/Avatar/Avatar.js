import React from "react";
import { Typography, Grid } from "@material-ui/core";
import styled from "styled-components";

// import AvatarImg from "../../images/avatar.jpeg";

// const StyledAvatarImg = styled.img`
//   width: 100%;
//   max-width: 200px;
// `;

const StyledAvatar = styled(Grid).attrs({
  justify: "center",
  alignItems: "center"
})`
  height: 200px;
  border: solid 2px ${props => props.theme.palette.primary.main};
`;

class Avatar extends React.PureComponent {
  render() {
    return (
      <StyledAvatar item container>
        <Typography>Avatar</Typography>
      </StyledAvatar>
    );
  }
}
// const Avatar = () => <StyledAvatarImg alt="avatar" src={AvatarImg} />;

export default Avatar;
