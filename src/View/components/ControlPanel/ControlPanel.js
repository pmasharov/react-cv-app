import React from "react";
import { Grid, Typography } from "@material-ui/core";
import Avatar from "../Avatar/Avatar";

// REF Component type
class AppWrapper extends React.PureComponent {
  render() {
    return (
      <Grid item container>
        <Avatar />
        <Typography variant="h5" component="h5">
          Pavel Masharov
        </Typography>
      </Grid>
    );
  }
}

export default AppWrapper;
