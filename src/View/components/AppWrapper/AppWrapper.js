import React from "react";
import { Container, Grid } from "@material-ui/core";
import MainContainer from "../MainContainer/MainContainer";
import ControlPanel from "../ControlPanel/ControlPanel";

// REF Component type
class AppWrapper extends React.PureComponent {
  render() {
    return (
      <Container>
        <Grid item container>
          <MainContainer />
          <ControlPanel />
        </Grid>
      </Container>
    );
  }
}

export default AppWrapper;
