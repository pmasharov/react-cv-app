import React from "react";
import ReactDOM from "react-dom";
import Launcher from "./Launcher";

ReactDOM.render(<Launcher />, document.getElementById("root"));
